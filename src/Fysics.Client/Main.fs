module Fysics.Client.Main

open Elmish
open Bolero
open Bolero.Html

type Page =
    | Home
    | Counter

type Model =
    { x: string
      value: int
      page: Page }

let initModel =
    { x = ""
      value = 0
      page = Home }

type Message =
    | Ping
    | SetPage of Page


let update message model =
    match message with
    | Ping -> model
    | SetPage page -> { model with page = page }

let view model dispatch = div [] [ button [ on.click (fun _ -> dispatch (SetPage Counter)) ] [ text "Counter" ] ]

let router = Router.infer SetPage (fun m -> m.page)

type MyApp() =
    inherit ProgramComponent<Model, Message>()

    override this.Program = Program.mkSimple (fun _ -> initModel) update view |> Program.withRouter router
